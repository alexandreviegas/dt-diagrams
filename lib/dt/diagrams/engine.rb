module Dt
  module Diagrams
    class Engine < ::Rails::Engine
      isolate_namespace Dt::Diagrams
    end
  end
end
