Dt::Diagrams::Engine.routes.draw do

  resource :erds, only: :show
  post "/erds", controller: :erds, action: :save, as: :save

end
