$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "dt/diagrams/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "dt-diagrams"
  s.version     = Dt::Diagrams::VERSION
  s.authors     = ["Alexandre Viegas"]
  s.email       = ["alexandreviegas@gmail.com"]
  s.homepage    = "http://www.alexandreviegas.com/rails/diagrams"
  s.summary     = "Shows some diagrams relative to your rails application like ERD"
  s.description = "Shows some diagrams relative to your rails application like ERD"
  s.license     = "MIT"

  s.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.rdoc"]
  s.test_files = Dir["test/**/*"]

  s.add_dependency "libxml-ruby", "~> 2.7.0"
end
