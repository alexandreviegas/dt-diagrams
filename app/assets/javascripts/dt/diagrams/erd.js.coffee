$ ->

  canvas = $('canvas')[0]
  context = canvas.getContext('2d')

  windowWidth = $(document).width() - 20
  windowHeight = $(document).height() - 60
  
  context.canvas.width = windowWidth
  context.canvas.height = windowHeight
  
  $('.entity').draggable({
    drag:( event, ui ) ->
      context.clearRect 0, 0, windowWidth, windowHeight
      $('#position').html(ui.offset.left + "," + ui.offset.top)
      drawAssociations context
  })

  $('.entity').each ->
    posX = $(this).children('.entity-position-x').html()
    posY = $(this).children('.entity-position-y').html()
    if posX != '' && posY != ''
      $(this).offset({left:posX, top:posY})

  context.clearRect 0, 0, windowWidth, windowHeight
  drawAssociations context

  $('#erd_save').click ->
    entities = []
    $('.entity').each ->
      entity_name = $(this).children('.entity-name').html()
      posX = $(this).offset().left
      posY = $(this).offset().top
      entities.push( entity = {name: entity_name, posX: posX, posY: posY})

    $.post "/diagrams/erds/", entities = {entities}, {} ,"json"

  $('a[data-action="showAttributes"]').click ->
    showAttributes $(this).attr('data-entity')

  $('a[data-action="hideAttributes"]').click ->
    hideAttributes $(this).attr('data-entity')

hideAttributes = (entity) ->
  if entity?
    $("#" + entity + " .entity-attributes").hide()
  else
    $(".entity-attributes").hide()

showAttributes = (entity) ->
  if entity?
    $("#" + entity + " .entity-attributes").show()
  else
    $(".entity-attributes").show()

drawAssociations = (context) ->
  $('.attribute-belongs-to').each ->
    belongs_to = $(this)
    
    entity_name = belongs_to.html()
    entity = $('div[id="'+entity_name+'"]')
    attribute = belongs_to.parent().children('.attribute-name')

    drawAssociationLine context, attribute, entity

drawAssociationLine = (context, attribute, entity) ->

  outterFromWidth = (attribute.width() / 2) + 20
  outterToWidth = (entity.width() / 2) + 20
  
  positionFrom = attributeMiddlePosition attribute
  positionTo = entityEndPointPosition entity

  initPosX = positionFrom[0]
  initPosY = positionFrom[1]
  endPosX = positionTo[0]
  endPosY = positionTo[1]

  #main line
  
  initJointPointX = if initPosX > endPosX then initPosX - outterFromWidth else initPosX + outterFromWidth
  endJointPointX = if endPosX > initPosX then endPosX - outterToWidth + 5 else endPosX + outterToWidth 

  context.beginPath()
  context.moveTo initPosX, initPosY
  context.lineTo initJointPointX, initPosY
  context.lineTo endJointPointX, endPosY
  context.lineTo endPosX, endPosY
  context.stroke()
  
  #initPos termination line
  terminationInitPosX = if initPosX > endPosX then initJointPointX + 5 else initJointPointX - 5
  context.beginPath()
  context.moveTo(terminationInitPosX, initPosY + 5)
  context.lineTo(terminationInitPosX, initPosY - 5)
  context.stroke()

  #endPos termination line
  terminationEndPosX = if initPosX > endPosX then endJointPointX - 5 else endJointPointX + 5
  context.beginPath()
  context.moveTo(terminationEndPosX, endPosY + 5)
  context.lineTo(terminationEndPosX, endPosY - 5)
  context.stroke()

attributeMiddlePosition = (div) ->
  posX = div.offset().left + (div.width() / 2)
  posY = div.offset().top + (div.height() / 2)
  
  middlePos = [posX, posY]

entityEndPointPosition = (div) ->
  posX = div.offset().left + (div.width() / 2)
  posY = div.offset().top + 10
  
  middlePos = [posX, posY]