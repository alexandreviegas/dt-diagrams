require_dependency "dt/diagrams/application_controller"
module Dt
  module Diagrams
    require 'xml'
    class ErdsController < ApplicationController

      FILE_PATH = "#{Rails.root}/public/erd.xml"

      def show
        @entities = []
        attributes = nil

        ActiveModel.eager_load!

        models = ActiveRecord::Base.descendants
        models.delete(ActiveRecord::SchemaMigration)

        load_xml_file
        xml_dom = parse_xml.parse

        models.each do |model|
          posX, posY = nil
          xml_dom.find("/entities/entity").each do |entity|
            if entity[:name] == model.to_s.underscore
              posX = entity[:positionX]
              posY = entity[:positionY]
            end
          end

          #check for has_many associations
          hm_associations = model.reflect_on_all_associations(:has_many)
          hm = hm_associations.map(&:name)

          @entities << {model: model, attributes: process_attributes(model), has_many: hm, posX: posX, posY: posY}

        end
        @entities
      end

      def save
        xml_dom = load_xml_file
        write_to_xml(xml_dom, params["entities"])
        render js: "alert('Diagrams positions saved successfully');"
      end

      private

        def has_belongs_to?(attribute, belongs_to)
          attribute.rindex("_id") != nil && belongs_to.index(attribute.sub("_id","").to_sym) != nil
        end

        def process_attributes(model)
          
          bt_associations = model.reflect_on_all_associations(:belongs_to)
          ho_associations = model.reflect_on_all_associations(:has_one)
          habm_associations = model.reflect_on_all_associations(:has_and_belongs_to_many)
          bt = nil
          attributes = []

          model::attribute_names.each do |attribute|
            #check for belongs_to associations
            if attribute.rindex("_id") != nil && bt_associations.map(&:name).index(attribute.sub("_id","").to_sym) != nil
              index = bt_associations.map(&:name).index(attribute.sub("_id","").to_sym)
              bt = attribute.sub("_id","")
              if !bt_associations.fetch(index).options.empty? and !bt_associations.fetch(index).options[:class_name].nil?
                bt = bt_associations.fetch(index).options[:class_name].underscore
              end
            else
              bt = nil
            end
            attributes << {name: attribute, belongs_to: bt}
          end

          attributes
        end

        #add a list of attributes to the node
        #the attributes formal parameter is a hash
        #with "name" and "value" as
        #key, value pairs
        def add_attributes( node, attributes )
          attributes.each do |name, value|
            XML::Attr.new( node, name.to_s, value )
          end
        end

        #create a node with name
        #and a hash of namespaces or attributes
        #passed to options
        def create_node( name, options=nil)
          node = XML::Node.new( name )

          attributes = options.delete( :attributes ) if options
          add_attributes( node, attributes ) if attributes
          node
        end

        def load_xml_file
          doc = nil
          if !File.exists?(FILE_PATH)
            doc = XML::Document.new
            doc.encoding = XML::Encoding::UTF_8
            write_to_xml(doc)
          else
            doc = XML::Document.file(FILE_PATH)
          end
          doc
        end

        def parse_xml
          XML::Parser.file(FILE_PATH, options: XML::Parser::Options::NOBLANKS)
        end

        def write_to_xml(doc, models=nil)

          root = create_node("entities")
          doc.root = root

          if models
            models.each do |model|
              name = model[1]["name"]
              posX = model[1]["posX"]
              posY = model[1]["posY"]
              entity = create_node("entity", attributes: {name: name, positionX: posX, positionY: posY})
              root << entity
            end
          else
            entity = create_node("entity", attributes: {name: "none", positionX: "0", positionY: "0"})
            root << entity
          end

          doc.save(FILE_PATH, indent: true)
        end

    end
  end
end